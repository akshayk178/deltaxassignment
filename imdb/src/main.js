import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ViewDesign from 'view-design'
import 'view-design/dist/styles/iview.css'
import iView from 'iview';
import locale from 'iview/dist/locale/en-US';
import axios from 'axios'
import {store} from './store/store.js'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


Vue.config.productionTip = false
Vue.use(ViewDesign)

Vue.use(iView, {locale: locale});
// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

//axios.defaults.baseURL = "https://localhost:44354/"
Vue.filter('to-uppercase',function(value){
   return value.toUpperCase()
},
);
Vue.filter('date',function(value){
  var dateArr = value.toString().split('-')
  var yy = dateArr[0]
  var mm = dateArr[1]
  var dd = dateArr[2].split('T')[0]
  const dateStr = dd+ '/'+mm+'/'+yy
  return dateStr
})

new Vue({
  router,
  render: h => h(App),
  store
}).$mount('#app')
