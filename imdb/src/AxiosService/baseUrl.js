export const movieUrl = "https://localhost:44354/movies/"
export const actorUrl = "https://localhost:44354/actors/"
export const producerUrl = "https://localhost:44354/producers/"
export const genreUrl = "https://localhost:44354/genres/"
