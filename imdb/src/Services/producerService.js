import axios from 'axios'
import { producerUrl } from '../AxiosService/baseUrl'
var producerService = {
    getProducers(){
      const promise = axios.get(producerUrl)
      return promise
    },
    addProducer(producerObj){
      const promise = axios.post(producerUrl,producerObj)
      return promise
    },
    updateProducer(producerObj){
        var url = producerUrl+ producerObj.id 
          const promise = axios.put(url,producerObj)
          return promise
    },
    deleteProducer(id){
      var url = producerUrl + id
      const promise = axios.delete(url)
      return promise
    }
}
export default producerService