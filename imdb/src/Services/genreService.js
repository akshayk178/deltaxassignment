import axios from 'axios'
import { genreUrl } from "../AxiosService/baseUrl";
const genreService = {
    getGenres(){
        const promise = axios.get(genreUrl)
        return promise
      },
}
export default genreService