import axios from 'axios'
import { movieUrl } from "../AxiosService/baseUrl";

var movieService = {
    getMovies(){
        const promise = axios.get(movieUrl)
        return promise
    },
    addMovie(movieRequest){
        const promise = axios.post(movieUrl,movieRequest)
        return promise
    },
    updateMovie(id,movieRequest){
        const url =movieUrl + id; 
        const promise= axios.put(url,movieRequest)
        return promise
    },
    deleteMovie(id){
        var url= movieUrl+ id                
        const promise = axios.delete(url)
        return promise
    }
}
export default movieService