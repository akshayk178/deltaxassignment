import axios from 'axios'
import {baseUrl} from '../AxiosService/baseUrl'
import {actorUrl} from '../AxiosService/baseUrl'

var actorService = {
  getActors(){
    const promise = axios.get(actorUrl)
    return promise
  },
  
  addActor(actorObj){
     const promise = axios.post(actorUrl,actorObj)
     return promise
  },

  updateActor(actorObj){
      var url = actorUrl+ actorObj.id 
      const promise = axios.put(url,actorObj)
      return promise       
  },

  deleteActor(id){
    var url= actorUrl + id
    const promise = axios.delete(url)
      return promise
  }
}
 export default actorService