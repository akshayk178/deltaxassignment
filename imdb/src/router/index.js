import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Movies from '../views/Movies.vue'
import Actors from '../views/Actors.vue'
import Producers from '../views/Producers.vue'
import MovieForm from '../views/MovieForm.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/movies',
    name: 'Movies',
    component: Movies
  },
  {
    path: '/actors',
    name: 'Actors',
    component: Actors
  },
  {
    path: '/producers',
    name: 'Producers',
    component: Producers
  },
  {
    path: '/addmovie',
    name: 'AddMovie',
    component: MovieForm
  },
  {
    path: '/editmovie',
    name: 'EditMovie',
    component: MovieForm
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
