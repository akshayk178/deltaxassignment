import vue from 'vue'
import Vuex from 'vuex'

vue.use(Vuex)

export const store = new Vuex.Store({
    state:{
        actorsLst:[],
        producersLst:[],
        genresLst:[],
        movieLst:[],
        movie:{}
    },
    getters:{
        actorsLst: state=>{
            return state.actorsLst
        },
        producersLst: state=>{
            return state.producersLst
        },
        genresLst: state=>{
            return state.genresLst
        },
        movieLst: state=>{
            return state.movieLst
        },
        movie: state=>{
            return state.movie
        }
    },
    mutations:{
        getActors:(state,data)=>{
            state.actorsLst.length = 0
                    for(let i in data)
                    {
                        const m = data[i]
                        state.actorsLst.push(m)
                    } 
        },
      deleteActor:(state,id)=>{
          for (let i in state.actorsLst){
              if (state.actorsLst[i].id == id){
                  state.actorsLst.splice(i,1)
              } 
          }
        
      },
      updateActor:(state,actorObj)=>{
        for(let i in state.actorsLst){
            if(state.actorsLst[i].id == actorObj.id){
                state.actorsLst.splice(i,1,actorObj)
            }
        }
        //updating actor in moviesList
        for(let i in state.movieLst){
            for(let j in state.movieLst[i].actors){
                if(state.movieLst[i].actors[j].id== actorObj.id){
                    state.movieLst[i].actors.splice(j,1,actorObj)
                }
            }
        } 
      },
      getProducers: (state,data)=> {
        state.producersLst.length = 0
        for(let i in data)
        {
            const m = data[i]
            state.producersLst.push(m)
        }
      },
      deleteProducer:(state,producerId)=>{
        for(let i in state.producersLst){
            if(state.producersLst[i].id == producerId){
                state.producersLst.splice(i,1)
                break
            }
        }
    },
    updateProducer:(state,producerObj)=>{
        for(let i in state.producersLst){
            if(state.producersLst[i].id == producerObj.id){ 
                 state.producersLst.splice(i,1,producerObj) 
                 break     
            }
        }
        for(let i in state.movieLst){
            var movieProducer = state.movieLst[i].producer
            if(movieProducer != null){
                if(movieProducer.id == producerObj.id){
                    state.movieLst[i].producer = producerObj
                    break
                }
            }
        }    
    },
    getGenres: (state,data)=>{
            state.genresLst.length = 0            
            for(let i in data)
            {
                const p = data[i]
                state.genresLst.push(p)
            }
    },
    getMovie: (state,id)=>{
        state.movie= {}
        state.movie= state.movieLst.find(x=> x.id==id)
        console.log(state.movie) 
    },
    getMovies: (state,data)=>{
            state.movieLst.length = 0
            for(let i in data)
            {
                const m = data[i]
                state.movieLst.push(m)
            }
    },
    deleteMovie:(state,mId) => {
        for(let i in state.movieLst){
            if(state.movieLst[i].id == mId){
                state.movieLst.splice(i,1)
                break
            }
        }
      
    },
    updateMovie:(state,payload)=> {
        for(let i in state.movieLst){
            if(state.movieLst[i].id == payload.id){
                state.movieLst[i].title = payload.movieRequest.title
                state.movieLst[i].yearOfRelease = payload.movieRequest.yearOfRelease
                state.movieLst[i].plot = payload.movieRequest.plot
                state.movieLst[i].poster = payload.movieRequest.poster
                state.movieLst[i].producer = payload.producer
                state.movieLst[i].actors = payload.actors
                state.movieLst[i].genres = payload.genres 
                break
            }
        }
    }
    },
    actions:{
        getActors: ({commit},data)=>{
            commit('getActors',data)
        },
        deleteActor: ({commit},aId)=>{
                commit('deleteActor',aId)
        },
        updateActor: ({commit},actorObj)=> {
            commit('updateActor',actorObj)
        },
        getProducers: ({commit},data)=> {
            commit('getProducers',data)
        },
        deleteProducer: ({commit},pId)=>{
                commit('deleteProducer',pId)
        },
        updateProducer: ({commit},producerOnj)=>{
            commit('updateProducer',producerOnj)
        },
        getGenres: ({commit},data)=>{
            commit('getGenres',data)
        },
        getMovie: ({commit},id)=>{
            commit('getMovie',id)
        },
        getMovies: ({commit},data)=> {
            commit('getMovies',data)
           
        },
        deleteMovie: ({commit},mId)=>{
                commit('deleteMovie',mId)
        },
        updateMovie:({commit},movieRequest)=>{
            commit('updateMovie',movieRequest)
        },
       
    }, 
}
)